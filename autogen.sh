#!/bin/sh

die()
{
	echo $@ 1>&2
	exit 1
}

aclocal    -I m4         || die "Failed to run aclocal."
autoheader               || die "Failed to run autoheader."
libtoolize --copy        || die "Failed to run libtoolize."
automake   --add-missing || die "Failed to run automake."
autoconf                 || die "Failed to run autoconf."
