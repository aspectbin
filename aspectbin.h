/* AspectBin - A GTK+ container for packing with consrained aspect ratio.
 * Copyright (C) 2009 Nick Bowler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef ASPECTBIN_H_
#define ASPECTBIN_H_

#include <gtk/gtk.h>

#define ASPECT_BIN_TYPE (aspect_bin_get_type())
#define ASPECT_BIN(obj) GTK_CHECK_CAST((obj), ASPECT_BIN_TYPE, AspectBin)
#define ASPECT_BIN_CLASS(class) \
	GTK_CHECK_CLASS_CAST((class), ASPECT_BIN_TYPE, AspectBinClass)
#define IS_ASPECT_BIN(obj) GTK_CHECK_TYPE((obj), ASPECT_BIN_TYPE)
#define IS_ASPECT_BIN_CLASS(class) \
	GTK_CHECK_CLASS_TYPE((class), ASPECT_BIN_TYPE, AspectBinClass)

typedef struct AspectBin      AspectBin;
typedef struct AspectBinClass AspectBinClass;

struct AspectBin {
	GtkContainer parent;

	GtkWidget *body, *side;
	gfloat     body_align, side_align;
	gfloat     ratio;

	gboolean   constrain;
	gboolean   fill;
};

struct AspectBinClass {
	GtkBinClass parent_class;
};

GType aspect_bin_get_type(void);
GtkWidget *aspect_bin_new(void);
void aspect_bin_set_body(AspectBin *, GtkWidget *, gfloat);
void aspect_bin_set_side(AspectBin *, GtkWidget *);
GtkWidget *aspect_bin_get_body(AspectBin *abin);
GtkWidget *aspect_bin_get_side(AspectBin *abin);

/* Registers widgets with libglade - primarily for static linking. */
void aspect_bin_register_widgets(void);

#endif
