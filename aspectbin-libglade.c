/* AspectBin - A GTK+ container for packing with consrained aspect ratio.
 * Copyright (C) 2009 Nick Bowler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glade/glade.h>
#include <glade/glade-build.h>
#include "aspectbin.h"

void aspect_bin_register_widgets(void)
{
	glade_register_widget(ASPECT_BIN_TYPE, glade_standard_build_widget,
	                                       glade_standard_build_children,
                                               NULL);
	glade_provide("aspectbin");
}

void glade_module_register_widgets(void)
{
	aspect_bin_register_widgets();
}
