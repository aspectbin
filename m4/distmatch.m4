dnl DIST_FIND_MATCH(outvar, pkg_prefix, pkg_dest)
AC_DEFUN([DIST_FIND_MATCH],
[
arg=`printf '%s\n' "$2" | sed 's:@<:@@:>@@<:@^$.*\/@:>@:\\\\&:g'`
$1=`printf '%s\n' "$3" | sed 's/^\/*'"$arg"'\/*//
	t foo
	: foo
	t; s:^etc/:${sysconfdir}/:
	t; s:^lib@<:@0-9@:>@*/:${libdir}/:
	t; s:^bin/:${bindir}/:
	t; s:^sbin/:${sbindir}/:
	t; s:^share/:${datarootdir}/:
	t; s:^:${prefix}/:
'`
])
