dnl PKG_CHECK_VAR(prefix, var, desc, pkg, [action-if-ok], [action-if-fail])
AC_DEFUN([PKG_CHECK_VAR],
[AC_REQUIRE([PKG_PROG_PKG_CONFIG])dnl
AC_ARG_VAR(m4_toupper([$1][_$2]), [$3, overriding pkg-config])dnl

m4_define([_PKG_VAR_TEXT], [dnl
Alternately, you may set the environment variable m4_toupper([$1][_$2]) to
avoid the need to call pkg-config.
See the pkg-config man page for more details.])

pkg_fail=no
AC_MSG_CHECKING([for $3])
if test x"$[]m4_toupper([$1][_$2])" = x; then
	_PKG_CONFIG([$1][_$2], [variable=$2], [$4])
else
	pkg_cv_$1[]_$2="$[]m4_toupper([$1][_$2])"
fi

if test x"[$pkg_cv_$1][_$2]" = x; then
	if test $pkg_failed = yes; then
		_PKG_SHORT_ERRORS_SUPPORTED
		if test $_pkg_short_errors_supported = yes; then
			$1[]_PKG_ERRORS=`$PKG_CONFIG --short-errors --print-errors "$4" 2>&1`
		else
			$1[]_PKG_ERRORS=`$PKG_CONFIG --print-errors "$4" 2>&1`
		fi
		echo "$$1[]_PKG_ERRORS" >&AS_MESSAGE_LOG_FD

		ifelse([$6], ,
			[AC_MSG_ERROR([dnl
Package requirements ($4) were not met:

$$1_PKG_ERRORS

Consider adjusting the PKG_CONFIG_PATH environment variable if you installed
software in a non-standard prefix.

_PKG_VAR_TEXT
])],
			[AC_MSG_RESULT([unknown])
			$6])
	elif test $pkg_failed = untried; then
		ifelse([$6], ,
			[AC_MSG_FAILURE([dnl
The pkg-config script could not be found or is too old.  Make sure it is in
your PATH or set the PKG_CONFIG environment variable to the full path to
pkg-config.

_PKG_VAR_TEXT

To get pkg-config, see <http://pkg-config.freedesktop.org/>.])],
			[$6])
	else
		ifelse([$6], ,
			[AC_MSG_ERROR([dnl
The variable ``$2'' was not defined in the package description database for dnl
m4_apply([m4_car], m4_split([$4])).

Consider adjusting the PKG_CONFIG_PATH environment variable if you installed
software in a non-standard prefix.

_PKG_VAR_TEXT
])],
			[AC_MSG_RESULT([unknown])
			$6])
	fi
else
	$1[]_$2=$pkg_cv_$1[]_$2
	AC_MSG_RESULT([$$1][_$2])
	ifelse([$5], , :, [$5])
fi
])
