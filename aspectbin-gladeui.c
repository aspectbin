/* AspectBin - A GTK+ container for packing with consrained aspect ratio.
 * Copyright (C) 2009 Nick Bowler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <gladeui/glade.h>
#include "aspectbin.h"

void aspect_bin_post_create(GladeWidgetAdaptor *adaptor,
                            GObject            *object,
                            GladeCreateReason   reason)
{
	AspectBin *abin;
	g_return_if_fail(IS_ASPECT_BIN(object));
	abin = ASPECT_BIN(object);

	if (reason == GLADE_CREATE_USER) {
		if (!aspect_bin_get_body(abin))
			aspect_bin_set_body(abin, glade_placeholder_new(), 1);
		if (!aspect_bin_get_side(abin))
			aspect_bin_set_side(abin, glade_placeholder_new());
	}
}
