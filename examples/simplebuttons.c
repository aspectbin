/* AspectBin - A GTK+ container for packing with consrained aspect ratio.
 * Copyright (C) 2009 Nick Bowler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <gtk/gtk.h>
#include <aspectbin.h>

int main(int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *aspectbin;
	GtkWidget *button1, *button2;

	gtk_init(&argc, &argv);

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);

	aspectbin = aspect_bin_new();
	button1 = gtk_button_new_with_label("Square");
	button2 = gtk_button_new_with_label("Rectangle");

	aspect_bin_set_body(ASPECT_BIN(aspectbin), button1, 1.0);
	aspect_bin_set_side(ASPECT_BIN(aspectbin), button2);

	g_object_set(G_OBJECT(aspectbin),
		"constrain", TRUE,
		NULL);
	gtk_container_child_set(GTK_CONTAINER(aspectbin), button1,
		"align", 0.5,
		NULL);

	gtk_container_add(GTK_CONTAINER(window), aspectbin);

	g_signal_connect (G_OBJECT(window), "destroy",
		G_CALLBACK(gtk_main_quit), NULL);

	gtk_widget_show_all(window);
	gtk_main();

	return 0;
}
