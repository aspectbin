/* AspectBin - A GTK+ container for packing with consrained aspect ratio.
 * Copyright (C) 2009 Nick Bowler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>

#include <glade/glade.h>
#include <gtk/gtk.h>

#include <aspectbin.h>

int main(int argc, char **argv)
{
	GladeXML *xml;
	GtkWidget *win;

	gtk_init(&argc, &argv);

	// Register the aspect bin widgets with libglade.
	aspect_bin_register_widgets();

	xml = glade_xml_new("simple.glade", NULL, NULL);
	if (!xml) {
		fprintf(stderr, "Failed to load simple.glade\n");
		return EXIT_FAILURE;
	}

	glade_xml_signal_autoconnect(xml);

	win = glade_xml_get_widget(xml, "mainWindow");
	gtk_widget_show_all(win);
	gtk_main();

	return 0;
}
